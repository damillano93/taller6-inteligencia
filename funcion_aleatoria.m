%% GRAFICA DE LA  MEDIANTE UNA RNA
% *************************************************************************
close all
clear all
clc
% Definimos las entradas y salidas deseadas para el entrenamiento y la
% validación
entradas_total = -20:1:20;
salidas_total = [0.0875 0.0002 0.0366 0.3178 0.8151 1.2631 1.1523 ...
           -0.4252 0.5477 1.2055 1.2309 0.7715 0.2621 0.0223 ...
           -0.0014 -0.119 -0.5309 -1.0681 -1.2962 -0.9002 0 ...
           0.9002 1.2961 1.0681 0.5309 0.119 0.0014 -0.0223 ...
           -0.2621 -0.7715 -1.2309 -1.2309 -1.2309 -1.2055 -0.5477 0.4252 1.1523 ...
           1.2631 0.8454 0.3178 0.03178 0.0366 -0.0002 -0.0875];
entradas = entradas_total(1:28);
salidas = salidas_total(1:28);
entradas_v =entradas_total(29:40);
salidas_v = salidas_total(29:40);
epoch_max = 10000;
capas_ocultas_max = 100;
for epoch = 1:100:epoch_max
%% Configuramos los parámetros de la red y graficamos

aleatoria = fitnet([7 1],"trainlm");
aleatoria.trainParam.epochs = epoch;
aleatoria.trainParam.goal = 0.0001;
[aleatoria,tr] = train(aleatoria,entradas,salidas);

Y = sim(aleatoria,entradas);
V = sim(aleatoria,entradas_v);

jframe = view(aleatoria);
%# create it in a MATLAB figure
hFig = figure('Menubar','none', 'Position',[100 100 565 166]);
jpanel = get(jframe,'ContentPane');
[~,h] = javacomponent(jpanel);
set(h, 'units','normalized', 'position',[0 0 1 1])

%# close java window
jframe.setVisible(false);
jframe.dispose();

%# print to file
set(hFig, 'PaperPositionMode', 'auto')
saveas(hFig, ['red_', num2str(epoch),'_epoch.png'])

%# close figure
close(hFig)
%%********************************************************************
subplot(2,2,1)
plot(entradas_v,salidas_v,'g');
grid on;
hold on;
plot(entradas_v,V,'ro');
hold off
title(['Validacion ' , num2str(epoch),' epoch'])
xlabel('x')
ylabel('y')
%%*********************************************************************
subplot(2,2,2)
plot(entradas,salidas,'g');
grid on;
hold on;
plot(entradas,Y,'ro');
hold off
title(['Entrenamiento ' , num2str(epoch),' epoch'])
xlabel('x')
ylabel('y')
subplot(2,2,[3,4])
plot(entradas,salidas,'g');
grid on;
hold on;
plot(entradas,Y,'ro');
hold on;
plot(entradas_v,V,'bx');
title(['Comportamiento ' , num2str(epoch),' epoch'])
xlabel('x')
ylabel('y')
hold off
saveas(gcf,['comportamiento_', num2str(epoch),'_epoch.png'])
end